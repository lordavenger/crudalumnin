<%-- 
    Document   : listar
    Created on : 24/06/2015, 07:04:12 AM
    Author     : RICARDO_UNT
--%>
<%@page import="java.util.ArrayList"%>
<%@page import="Modelos.Alumno"%>
<%HttpSession sesion; %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>LISTADO DE ALUMNOSxD</h1>
        <table border="1">
            <thead>
                <tr>
                    <th>Codigo</th>
                    <th>Nombre</th>
                    <th>Dni</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                <%
                    sesion=request.getSession(true);
                    if(sesion.getAttribute("sesAlumnos")!=null){
                        ArrayList<Alumno> lstAlumn=(ArrayList<Alumno>)sesion.getAttribute("sesAlumnos");
                        for(int i=0;i<lstAlumn.size();i++){
                %>
                <tr>
                    <td><%=lstAlumn.get(i).getCodigo() %></td>
                    <td><%=lstAlumn.get(i).getNombre()%></td>
                    <td><%=lstAlumn.get(i).getDni()%></td>
                    <td><a href="editar.jsp?codigoEditar=<%=lstAlumn.get(i).getCodigo() %>">Editar</a>
                        <a href="EliminarAlumno?indiceEliminar=<%=i %>">Eliminar</a></td>
                </tr>
                <%
                        }
                    }
                %>
            </tbody>
        </table>

        <a href="index.jsp">Registar</a>
    </body>
</html>
